using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacman : MonoBehaviour
{
    // Start is called before the first frame update
    private Rigidbody rb;
    private GameObject go;
    private int speed = 100;
    public bool isGrounded;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
      
            rb.velocity = new Vector3(Input.GetAxis("Horizontal") * speed, 0, Input.GetAxis("Vertical") * speed);
       
     }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Coin"))
        {
            GameObject.Destroy(go);
        }
        else
        {
           
        }
    }
    
}
